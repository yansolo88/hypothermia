﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class levelManager : MonoBehaviour {

    public void StartScene (string level) {
        SceneManager.LoadScene(level);
	}

	public void QuitGame () {
		Application.Quit();
	}
}
