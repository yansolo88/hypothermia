﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class PlayingFootsteps : MonoBehaviour
    {

        private CustomThirdPersonCharacter cc;
        private AudioSource As;

        // Use this for initialization
        void Start()
        {
            cc = transform.GetComponent<CustomThirdPersonCharacter>();
            As = transform.GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            if (cc.m_IsGrounded && cc.m_Rigidbody.velocity.magnitude > 0.2f && !(As.isPlaying))
            {
                As.pitch = Random.Range(0.8f, 1.1f);
                As.Play();
            }
        }
    }
}
