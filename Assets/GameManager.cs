﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private GameObject m_Player;

    public float velocity;

    private void Awake()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Use this for initialization
    void Start () {
	    	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Death()
    {
        SceneManager.LoadScene("gameHypothermia", LoadSceneMode.Single);
    }
}
